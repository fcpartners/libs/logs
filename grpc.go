package logs

import (
	"context"

	"github.com/sirupsen/logrus"
	"gitlab.com/fcpartners/libs/logs/log"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var GRPCServerCodes = []codes.Code{
	codes.Unknown,
	codes.Internal,
	codes.ResourceExhausted,
	codes.Unavailable,
	codes.DeadlineExceeded,
	codes.Unimplemented,
	codes.Unauthenticated,
}

func NewGrpcServerUnaryErrorLogger(logger log.Logger, logOnly ...codes.Code) grpc.UnaryServerInterceptor {
	return func(
		ctx context.Context,
		req interface{},
		info *grpc.UnaryServerInfo,
		handler grpc.UnaryHandler,
	) (resp interface{}, err error) {
		resp, err = handler(ctx, req)

		gStatus, _ := status.FromError(err)
		if shouldLog(gStatus.Code(), logOnly) {
			logger.WithFields(logrus.Fields{"details": gStatus.Details(), "code": gStatus.Code()}).Error(gStatus.Message())
		}

		return resp, err
	}
}

func shouldLog(search codes.Code, pass []codes.Code) bool {
	for _, val := range pass {
		if search == val {
			return true
		}
	}

	return false
}
