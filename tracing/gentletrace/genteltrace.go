package gentletrace

import (
	"context"
	"fmt"

	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"github.com/opentracing/opentracing-go/log"
	"gitlab.com/fcpartners/libs/logs/tracing"
	"gitlab.com/fcpartners/libs/logs/tracing/http"
	"gopkg.in/h2non/gentleman.v2"
	gc "gopkg.in/h2non/gentleman.v2/context"
	"gopkg.in/h2non/gentleman.v2/plugin"
)

const spanKey = "tracing-span-key"

type Plugin interface {
	plugin.Plugin
}

func NewPlugin(tracer tracing.Tracer, config tracing.Config) Plugin {
	plug := plugin.New()

	// Attach the middleware handler for before dial phase
	plug.SetHandler("before dial", func(ctx *gc.Context, h gc.Handler) {
		span := startSpanFromContextWithTracer(ctx, tracer, fmt.Sprintf("%s request: %s", ctx.Request.URL.Scheme, ctx.Request.URL.Path))
		ext.HTTPMethod.Set(span, ctx.Request.Method)
		ext.SpanKind.Set(span, "api-client")
		ext.HTTPUrl.Set(span, ctx.Request.URL.String())

		if config.LogPayloads {
			span.LogFields(
				log.String("request", http.ReadRequestString(ctx.Request)),
			)
		}

		h.Next(ctx)
	})

	plug.SetHandler("after dial", func(ctx *gc.Context, h gc.Handler) {
		span := spanFromContext(ctx)
		if span != nil {
			defer span.Finish()

			if ctx.Error != nil {
				ext.Error.Set(span, true)
				span.LogFields(
					log.Error(ctx.Error),
				)
			} else if ctx.Err() != nil {
				ext.Error.Set(span, true)
				span.LogFields(
					log.Error(ctx.Err()),
				)
			}

			if config.LogPayloads {
				span.LogFields(
					log.String("response", http.ReadResponseString(ctx.Response)),
				)
			}
		}

		h.Next(ctx)
	})

	return plug
}

func WithSpanFromContext(ctx context.Context, request *gentleman.Request) *gentleman.Request {
	span := opentracing.SpanFromContext(ctx)
	request.Context.Set(spanKey, span)

	return request
}

func spanFromContext(ctx *gc.Context) opentracing.Span {
	span, _ := ctx.Get(spanKey).(opentracing.Span)
	return span
}

// startSpanFromContextWithTracer is factored out for testing purposes.
func startSpanFromContextWithTracer(ctx *gc.Context, tracer tracing.Tracer, operationName string, opts ...opentracing.StartSpanOption) opentracing.Span {
	var span opentracing.Span
	if parentSpan := spanFromContext(ctx); parentSpan != nil {
		opts = append(opts, opentracing.ChildOf(parentSpan.Context()))
		span = tracer.StartSpan(operationName, opts...)
	} else {
		span = tracer.StartSpan(operationName, opts...)
	}
	ctx.Set(spanKey, span)

	return span
}
