package tracing

import (
	"context"
	"fmt"
	"io"
	"strconv"
	"time"

	"github.com/jinzhu/gorm"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"github.com/opentracing/opentracing-go/log"
	"github.com/uber/jaeger-client-go"
	"github.com/uber/jaeger-client-go/utils"

	gsLog "gitlab.com/fcpartners/libs/logs/log"
	"gitlab.com/fcpartners/libs/logs/tracing/tracegorm"
	"gitlab.com/fcpartners/libs/logs/tracing/traceorm"
	"gitlab.com/fcpartners/libs/orm"
)

type Sampler string

const samplingRefreshInterval = time.Second * 30

type (
	Config struct {
		Agent struct {
			Host    string
			Compact struct {
				Port uint16
			}
			Sampling struct {
				Port uint16
			}
		}
		LogPayloads bool
	}
	Tracer interface {
		ContextWithSpan(ctx context.Context, span opentracing.Span) context.Context
		SpanFromContext(ctx context.Context) opentracing.Span
		StartSpan(operationName string, opts ...opentracing.StartSpanOption) opentracing.Span
		StartSpanFromContext(ctx context.Context, operationName string, opts ...opentracing.StartSpanOption) (opentracing.Span, context.Context)
		Inject(sm opentracing.SpanContext, format interface{}, carrier interface{}) error
		Extract(format interface{}, carrier interface{}) (opentracing.SpanContext, error)
		Close() error
	}
	tracer struct {
		impl   opentracing.Tracer
		closer io.Closer
	}
	loggerAdapter struct {
		logger gsLog.Logger
	}
)

func (l loggerAdapter) Error(msg string) {
	l.logger.Error(msg)
}

func (l loggerAdapter) Infof(msg string, args ...interface{}) {
	l.logger.Infof(msg, args)
}

func SetSpanToGorm(ctx context.Context, db *gorm.DB) *gorm.DB {
	return tracegorm.SetSpanToGorm(ctx, db)
}

func SetSpanToOrm(ctx context.Context, db *orm.DB) *orm.DB {
	return traceorm.SetSpanToOrm(ctx, db)
}

func LogErrorToSpan(span opentracing.Span, err error) {
	ext.Error.Set(span, true)
	span.LogFields(log.Error(err))
}

func NewTracer(config *Config, serviceName string, logger gsLog.Logger) (Tracer, error) {
	logAdapt := loggerAdapter{logger: logger}

	if config.Agent.Compact.Port == uint16(0) {
		return &tracer{impl: opentracing.NoopTracer{}}, nil
	}

	sampler, err := newRemotelyControlledSampler(config, serviceName)
	if err != nil {
		return nil, err
	}

	reporter, err := newTracerReporter(fmt.Sprintf("%s:%d", config.Agent.Host, config.Agent.Compact.Port), logAdapt)
	if err != nil {
		return nil, err
	}

	impl, closer := jaeger.NewTracer(serviceName, sampler, reporter, jaeger.TracerOptions.Logger(logAdapt))

	return &tracer{impl: impl, closer: closer}, nil
}

func newRemotelyControlledSampler(config *Config, serviceName string) (jaeger.Sampler, error) {
	initialSampler, err := newConstSampler(true)
	if err != nil {
		return nil, err
	}

	sampler := jaeger.NewRemotelyControlledSampler(
		serviceName,
		jaeger.SamplerOptions.SamplingServerURL(fmt.Sprintf("http://%s:%d", config.Agent.Host, config.Agent.Sampling.Port)),
		jaeger.SamplerOptions.InitialSampler(initialSampler),
		jaeger.SamplerOptions.SamplingRefreshInterval(samplingRefreshInterval),
	)

	return sampler, nil
}

func newConstSampler(param interface{}) (jaeger.Sampler, error) {
	sample, err := strconv.ParseBool(fmt.Sprintf("%v", param))
	if err != nil {
		return nil, err
	}

	return jaeger.NewConstSampler(sample), nil
}

func newTracerReporter(dsn string, logger loggerAdapter) (jaeger.Reporter, error) {
	sender, err := jaeger.NewUDPTransport(dsn, utils.UDPPacketMaxLength)
	if err != nil {
		return jaeger.NewNullReporter(), fmt.Errorf("failed to init UDP transport: %v", err)
	}

	return jaeger.NewRemoteReporter(sender, jaeger.ReporterOptions.Logger(logger)), err
}

func (t *tracer) ContextWithSpan(ctx context.Context, span opentracing.Span) context.Context {
	return opentracing.ContextWithSpan(ctx, span)
}

func (t *tracer) StartSpan(operationName string, opts ...opentracing.StartSpanOption) opentracing.Span {
	return t.impl.StartSpan(operationName, opts...)
}

func (t *tracer) SpanFromContext(ctx context.Context) opentracing.Span {
	return opentracing.SpanFromContext(ctx)
}

func (t *tracer) StartSpanFromContext(ctx context.Context, operationName string, opts ...opentracing.StartSpanOption) (opentracing.Span, context.Context) {
	var span opentracing.Span
	if parentSpan := opentracing.SpanFromContext(ctx); parentSpan != nil {
		opts = append(opts, opentracing.ChildOf(parentSpan.Context()))
		span = t.StartSpan(operationName, opts...)
	} else {
		span = t.StartSpan(operationName, opts...)
	}

	return span, opentracing.ContextWithSpan(ctx, span)
}

func (t *tracer) Inject(sm opentracing.SpanContext, format interface{}, carrier interface{}) error {
	return t.impl.Inject(sm, format, carrier)
}

func (t *tracer) Extract(format interface{}, carrier interface{}) (opentracing.SpanContext, error) {
	return t.impl.Extract(format, carrier)
}

func (t *tracer) Close() error {
	if t.closer == nil {
		return nil
	}

	return t.closer.Close()
}
