package traceorm

import (
	"context"

	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"github.com/opentracing/opentracing-go/log"
	"gitlab.com/fcpartners/libs/orm"
)

const (
	parentSpanGormKey = "opentracingParentSpan"
	spanGormKey       = "opentracingSpan"
)

// SetSpanToGorm sets span to gorm settings, returns cloned DB
func SetSpanToOrm(ctx context.Context, db *orm.DB) *orm.DB {
	if ctx == nil {
		return db
	}
	parentSpan := opentracing.SpanFromContext(ctx)
	if parentSpan == nil {
		return db
	}
	return db.Set(parentSpanGormKey, parentSpan)
}

func OpenSpan(db *orm.DB) *orm.DB {
	val, ok := db.Get(parentSpanGormKey)
	if !ok {
		return db
	}
	parentSpan := val.(opentracing.Span)
	tr := parentSpan.Tracer()
	sp := tr.StartSpan("sql", opentracing.ChildOf(parentSpan.Context()))
	ext.DBType.Set(sp, "sql")

	return db.Set(spanGormKey, sp)
}

func FinishSpan(db *orm.DB, tableName, operation, query string, err error) {
	val, ok := db.Get(spanGormKey)
	if !ok {
		return
	}
	sp := val.(opentracing.Span)

	if operation == "" {
		operation = "RAW"
	}

	if err != nil {
		if orm.ErrNoRows != err {
			ext.Error.Set(sp, true)
		}
		sp.LogFields(
			log.Error(err),
		)
	}

	sp.LogFields(
		log.String("db.statement", query),
		log.String("db.table", tableName),
		log.String("db.method", operation),
	)
	sp.Finish()

}

func FinishSpanWithOrm(db *orm.DB, tableName string, o orm.Ormer, err error) {
	val, ok := db.Get(spanGormKey)
	if !ok {
		return
	}
	sp := val.(opentracing.Span)

	e := GetError(o.GetError(), err)

	if e != nil {
		if orm.ErrNoRows != e {
			ext.Error.Set(sp, true)
		}
		sp.LogFields(
			log.Error(e),
		)
	}

	sp.LogFields(
		log.String("db.statement", o.GetQuery()),
		log.String("db.table", tableName),
		log.String("db.method", o.GetOperation()),
	)
	//fmt.Printf("%s:%s\n","db.statement", o.GetQuery())
	//fmt.Printf("%s:%s\n","db.table", tableName)
	//fmt.Printf("%s:%s\n","db.method", o.GetOperation())
	//if e != nil {
	//	fmt.Printf("%s:%s\n","db.error", e.Error())
	//}
	sp.Finish()
}

func GetError(err1, err2 error) error {
	if err1 != nil {
		return err1
	}
	if err2 != nil {
		return err2
	}
	return nil
}
