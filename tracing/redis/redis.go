package redis

import (
	"context"
	"fmt"

	"github.com/gomodule/redigo/redis"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"github.com/opentracing/opentracing-go/log"
	"gitlab.com/fcpartners/libs/logs/tracing"
)

type Pool interface {
	Get(ctx context.Context) redis.Conn
	Stats() redis.PoolStats
	ActiveCount() int
	IdleCount() int
	Close() error
}

type (
	tracedPool struct {
		pool   *redis.Pool
		tracer tracing.Tracer
	}
	tracedConn struct {
		conn   redis.Conn
		ctx    context.Context
		tracer tracing.Tracer
	}
)

func NewTracedPool(pool *redis.Pool, tracer tracing.Tracer) Pool {
	return &tracedPool{
		pool:   pool,
		tracer: tracer,
	}
}

func (t *tracedPool) Get(ctx context.Context) redis.Conn {
	return &tracedConn{
		ctx:    ctx,
		conn:   t.pool.Get(),
		tracer: t.tracer,
	}
}

func (t *tracedPool) Stats() redis.PoolStats {
	return t.pool.Stats()
}

func (t *tracedPool) ActiveCount() int {
	return t.pool.ActiveCount()
}

func (t *tracedPool) IdleCount() int {
	return t.pool.IdleCount()
}

func (t *tracedPool) Close() error {
	return t.pool.Close()
}

func (c *tracedConn) Close() error {
	return c.conn.Close()
}

func (c *tracedConn) Err() error {
	return c.conn.Err()
}

func (c *tracedConn) Do(commandName string, args ...interface{}) (interface{}, error) {
	span, _ := c.tracer.StartSpanFromContext(c.ctx, "redis::"+commandName)
	span = buildTags(span, args...)

	replpy, err := c.conn.Do(commandName, args...)
	if err != nil {
		ext.Error.Set(span, true)
		span.LogFields(
			log.Error(err),
		)
	}

	span.Finish()

	return replpy, err
}

func (c *tracedConn) Send(commandName string, args ...interface{}) error {
	span, _ := c.tracer.StartSpanFromContext(c.ctx, "redis::"+commandName)
	span = buildTags(span, args...)

	err := c.conn.Send(commandName, args...)
	if err != nil {
		ext.Error.Set(span, true)
		span.LogFields(
			log.Error(err),
		)
	}

	span.Finish()

	return err
}

func (c *tracedConn) Flush() error {
	return c.conn.Flush()
}

func (c *tracedConn) Receive() (reply interface{}, err error) {
	return c.conn.Receive()
}

func buildTags(span opentracing.Span, args ...interface{}) opentracing.Span {
	lenArgs := len(args)
	if len(args) > 0 {
		span.SetTag("key", args[0])
		if lenArgs > 2 {
			span.SetTag("params", fmt.Sprintf("%v", args[1:lenArgs-1]))
		} else if lenArgs > 1 {
			span.SetTag("param", fmt.Sprintf("%v", args[1]))
		}
	}

	return span
}
