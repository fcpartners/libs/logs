package graylog

import (
	"fmt"
	"os"
	"strings"

	"gopkg.in/gemnasium/logrus-graylog-hook.v2"

	"gitlab.com/fcpartners/libs/logs/log"
)

type (
	Config struct {
		Namespace string
		Project   string
		Host      string
		Port      uint16
	}
	HookManager interface {
		SetUp()
		TearDown()
	}
	hookManager struct {
		hook   *graylog.GraylogHook
		logger log.Logger
		config *Config
	}
)

func NewHookManager(config *Config, logger log.Logger) HookManager {
	return &hookManager{
		logger: logger,
		config: config,
	}
}

func (hm *hookManager) SetUp() {
	hm.logger.AddHook(newExtraFieldsHook(log.DebugLevel, hm.config.Namespace, hm.config.Project))
	hm.hook = graylog.NewAsyncGraylogHook(fmt.Sprintf("%s:%d", hm.config.Host, hm.config.Port), map[string]interface{}{})
	hm.logger.AddHook(hm.hook)
	hm.hook.Flush()
}

func (hm *hookManager) TearDown() {
	if hm.hook != nil {
		hm.hook.Flush()
	}
}

type extraFieldsHook struct {
	Level     log.Level
	Namespace string
	Project   string
}

func newExtraFieldsHook(level log.Level, namespace, project string) *extraFieldsHook {
	return &extraFieldsHook{
		Level:     level,
		Namespace: namespace,
		Project:   project,
	}
}

func (h *extraFieldsHook) Fire(entry *log.Entry) error {
	entry.Data["namespace"] = h.Namespace
	entry.Data["project"] = h.Project

	if entry.HasCaller() {
		funcVal := fmt.Sprintf("%s()", entry.Caller.Function)
		s := strings.Split(funcVal, h.Project)
		funcVal = s[len(s)-1]
		funcVal = strings.TrimPrefix(funcVal, string(os.PathSeparator))

		fileVal := fmt.Sprintf("%s:%d", entry.Caller.File, entry.Caller.Line)
		s = strings.Split(fileVal, h.Project)
		fileVal = s[len(s)-1]
		fileVal = strings.TrimPrefix(fileVal, string(os.PathSeparator))

		caller := ""
		if fileVal == "" {
			caller = funcVal
		} else if funcVal == "" {
			caller = fileVal
		} else {
			caller = fileVal + " " + funcVal
		}
		entry.Data["caller"] = caller
	}

	return nil
}

func (h *extraFieldsHook) Levels() []log.Level {
	var levels []log.Level
	for _, level := range log.AllLevels {
		if level <= h.Level {
			levels = append(levels, level)
		}
	}

	return levels
}
