package logs

import (
	"bytes"
	"fmt"
	"os"
	"runtime"
	"sort"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

const (
	//locationSkip = 1
	//locationTag = "%loc%"
	// Default log format will output [INFO]: 2006-01-02T15:04:05Z07:00 - Log message
	//defaultLogFormat       = "[%lvl%]: %time% - %msg%"
	defaultTimestampFormat = time.RFC3339
)

// formatter - logrus formatter, implements logrus.formatter
type formatter struct {
	Project         string
	FieldsOrder     []string // default: fields sorted alphabetically
	TimestampFormat string   // default: time.StampMilli = "Jan _2 15:04:05.000"
	HideKeys        bool     // show [fieldValue] instead of [fieldKey:fieldValue]
	NoColors        bool     // disable colors
	NoFieldsColors  bool     // color only level, default is level + fields
	ShowFullLevel   bool     // true to show full level [WARNING] instead [WARN]
	TrimMessages    bool     // true to trim whitespace on messages
	ShowWhere       bool
	//SkipLines       int
}

// Format an log entry
func (t *formatter) Format(entry *logrus.Entry) ([]byte, error) {
	levelColor := getColorByLevel(entry.Level)

	timestampFormat := t.TimestampFormat
	if timestampFormat == "" {
		timestampFormat = defaultTimestampFormat
	}

	// output buffer
	b := &bytes.Buffer{}

	// write time
	b.WriteString(entry.Time.Format(timestampFormat))

	// write level
	level := strings.ToUpper(entry.Level.String())

	if !t.NoColors {
		_, _ = fmt.Fprintf(b, "\x1b[%dm", levelColor)
	}

	b.WriteString(" [")
	if t.ShowFullLevel {
		b.WriteString(level)
	} else {
		b.WriteString(level[:4])
	}
	b.WriteString("] ")

	if !t.NoColors && t.NoFieldsColors {
		b.WriteString("\x1b[0m")
	}

	// write fields
	if t.FieldsOrder == nil {
		t.writeFields(b, entry)
	} else {
		t.writeOrderedFields(b, entry)
	}

	if !t.NoColors && !t.NoFieldsColors {
		b.WriteString("\x1b[0m")
	}

	// write message
	if t.TrimMessages {
		b.WriteString(strings.TrimSpace(entry.Message))
	} else {
		b.WriteString(entry.Message)
	}

	if t.ShowWhere {
		b.WriteString(t.where( /*t.SkipLines*/ ))
	}

	b.WriteByte('\n')
	return b.Bytes(), nil
}

func (t *formatter) writeFields(b *bytes.Buffer, entry *logrus.Entry) {
	if len(entry.Data) != 0 {
		fields := make([]string, 0, len(entry.Data))
		for field := range entry.Data {
			fields = append(fields, field)
		}

		sort.Strings(fields)

		for _, field := range fields {
			t.writeField(b, entry, field)
		}
	}
}

func (t *formatter) writeOrderedFields(b *bytes.Buffer, entry *logrus.Entry) {
	length := len(entry.Data)
	foundFieldsMap := map[string]bool{}
	for _, field := range t.FieldsOrder {
		if _, ok := entry.Data[field]; ok {
			foundFieldsMap[field] = true
			length--
			t.writeField(b, entry, field)
		}
	}

	if length > 0 {
		notFoundFields := make([]string, 0, length)
		for field := range entry.Data {
			if foundFieldsMap[field] == false {
				notFoundFields = append(notFoundFields, field)
			}
		}

		sort.Strings(notFoundFields)

		for _, field := range notFoundFields {
			t.writeField(b, entry, field)
		}
	}
}

func (t *formatter) writeField(b *bytes.Buffer, entry *logrus.Entry, field string) {
	if t.HideKeys {
		_, _ = fmt.Fprintf(b, "[%v] ", entry.Data[field])
	} else {
		_, _ = fmt.Fprintf(b, "[%s:%v] ", field, entry.Data[field])
	}
}

const (
	colorRed    = 31
	colorYellow = 33
	colorBlue   = 36
	colorGray   = 37
)

func getColorByLevel(level logrus.Level) int {
	switch level {
	case logrus.DebugLevel:
		return colorGray
	case logrus.WarnLevel:
		return colorYellow
	case logrus.ErrorLevel, logrus.FatalLevel, logrus.PanicLevel:
		return colorRed
	default:
		return colorBlue
	}
}

func (t *formatter) where( /*skip int*/ ) string {
	//pc, file, line, ok := runtime.Caller(skip)
	//if !ok {
	//	return "unknown:0:unknown()"
	//}
	//
	//// make file path where was error
	//pathElements := strings.Split(file, "/" /*string(os.PathSeparator)*/)
	//index := sliceIndex(pathElements, "src")
	//if index >= 0 {
	//	pathElements = pathElements[index+2:]
	//}
	//file = path.Join(pathElements...)
	//_ = pc
	//
	////return fmt.Sprintf("%s:%s(%d)", file, path.Base(runtime.FuncForPC(pc).Name()), line)
	//return fmt.Sprintf(" [caller: %s:%d]", file, line)

	c := func() (caller string) {
		caller = "unknown:0 unknown"

		// Ask runtime.Callers for up to 10 pcs, including runtime.Callers itself.
		pc := make([]uintptr, 15)
		n := runtime.Callers(0, pc)
		if n == 0 {
			// No pcs available. Stop now.
			// This can happen if the first argument to runtime.Callers is large.
			return
		}

		pc = pc[:n] // pass only valid pcs to runtime.CallersFrames
		frames := runtime.CallersFrames(pc)

		// Loop to get frames.
		// A fixed number of pcs can expand to an indefinite number of Frames.
		for {
			frame, more := frames.Next()

			// To keep this example's output stable
			// even if there are changes in the testing package,
			// stop unwinding when we leave package runtime.
			if strings.Contains(frame.File, t.Project+string(os.PathSeparator)) {
				funcVal := fmt.Sprintf("%s()", frame.Function)
				s := strings.Split(funcVal, t.Project)
				funcVal = s[len(s)-1]
				funcVal = strings.TrimPrefix(funcVal, string(os.PathSeparator))

				fileVal := fmt.Sprintf("%s:%d", frame.File, frame.Line)
				s = strings.Split(fileVal, t.Project)
				fileVal = s[len(s)-1]
				fileVal = strings.TrimPrefix(fileVal, string(os.PathSeparator))

				if fileVal == "" {
					caller = funcVal
				} else if funcVal == "" {
					caller = fileVal
				} else {
					caller = fileVal + " " + funcVal
				}
				return
			}

			if !more {
				break
			}
		}
		return
	}
	caller := c()
	//fmt.Printf("- | %s\n", caller )
	return fmt.Sprintf(" [caller: %s]", caller)
}

//func sliceIndex(slice interface{}, item interface{}) int {
//	s := reflect.ValueOf(slice)
//
//	if s.Kind() != reflect.Slice {
//		panic("1-st parameter given a non-slice type")
//	}
//
//	for i := 0; i < s.Len(); i++ {
//		if s.Index(i).Interface() == item {
//			return i
//		}
//	}
//	return -1
//}
