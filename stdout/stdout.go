package stdout

import (
	"io"
	"os"

	"gitlab.com/fcpartners/libs/logs/log"
)

func SetUp(logger log.Logger) {
	hook := newStdoutLogger(os.Stdout, log.WarnLevel, log.ErrorLevel, log.PanicLevel, log.FatalLevel, log.DebugLevel, log.InfoLevel)
	logger.AddHook(hook)
}

func newStdoutLogger(writer io.Writer, levels ...log.Level) Logger {
	return Logger{
		levels: levels,
		writer: writer,
	}
}

type Logger struct {
	levels []log.Level
	writer io.Writer
}

func (logger Logger) Levels() []log.Level {
	return logger.levels
}

func (logger Logger) Fire(entry *log.Entry) error {
	formatted, err := entry.String()
	if err != nil {
		return err
	}

	_, err = logger.writer.Write([]byte(formatted))

	return err
}
