package monitoring

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"sync"
	"time"

	"github.com/dimiro1/health"
	"github.com/labstack/echo"
	"github.com/pkg/errors"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gitlab.com/fcpartners/libs/logs/log"
	"gitlab.com/fcpartners/libs/logs/monitoring/checkers"
	"gitlab.com/fcpartners/libs/logs/monitoring/metrics"
)

const (
	HealthEndpoint      = "/health"
	EchoShutdownTimeout = 5 * time.Second
)

var (
	ErrServerNotSpecified       = fmt.Errorf("web server is not specified")
	ErrServerAlreadyStarted     = fmt.Errorf("server already started")
	ErrEndpointsIsNotConfigured = fmt.Errorf("unable to use monitoring without any endpoint")
	ErrEmptyHandler             = fmt.Errorf("unable to add nil handler to monitoring")
)

type Config struct {
	Host string
	Port uint16
}

type (
	Monitor interface {
		TrackMetrics(metrics ...metrics.MetricTracker) Monitor
		TrackHealth(handler *HealthHandler) Monitor
		StartDetached(logger log.Logger)
		Start() error
		GracefulStop() error
		Error() error
	}
	HealthHandler struct {
		health.CompositeChecker
		logger log.Logger
	}
	monitor struct {
		config  *Config
		echo    *echo.Echo
		handler *HealthHandler

		useMetrics      bool
		useHealth       bool
		isServerStarted bool

		err          error
		serverErrors chan error
		once         sync.Once
	}
)

// NewHandler returns a new Handler
func NewHealthHandler(logger log.Logger) *HealthHandler {
	return &HealthHandler{
		logger: logger,
	}
}

func New(config Config) Monitor {
	return &monitor{
		config:       &config,
		serverErrors: make(chan error),
	}
}

// ServeHTTP returns a json encoded Health
// set the status to http.StatusServiceUnavailable if the check is down
func (h *HealthHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")

	checker := h.CompositeChecker.Check()

	if checker.IsDown() {
		w.WriteHeader(http.StatusServiceUnavailable)
		body, err := json.Marshal(checker)
		if err != nil {
			h.logger.Error(errors.Wrap(err, "Health check encode failed"))
		}
		h.logger.Error(errors.New(string(body)))
	}

	if err := json.NewEncoder(w).Encode(checker); err != nil {
		h.logger.Error(errors.Wrap(err, "Health check encode failed"))
	}
}

func (m *monitor) TrackMetrics(metrics ...metrics.MetricTracker) Monitor {
	if m.err != nil {
		return m
	}

	for _, tracker := range metrics {
		err := tracker.SetUp()
		if err != nil {
			m.err = errors.Wrap(err, "track metrics")
			return m
		}
	}

	m.useMetrics = true

	return m
}

func (m *monitor) TrackHealth(handler *HealthHandler) Monitor {
	if m.err != nil {
		return m
	}

	if handler == nil {
		m.err = errors.Wrap(ErrEmptyHandler, "track health")
		return m
	}

	m.handler = handler
	m.useHealth = true
	return m
}

func (m *monitor) StartDetached(logger log.Logger) {
	go func(logger log.Logger) {
		if err := m.Start(); err != nil {
			logger.Warning(err)
		}
	}(logger)
}

func (m *monitor) Start() error {
	if m.err != nil {
		return m.err
	}

	if m.isServerStarted {
		m.err = errors.Wrap(ErrServerAlreadyStarted, "server start")
		return m.err
	}

	if !m.useMetrics && !m.useHealth {
		m.err = errors.Wrap(ErrEndpointsIsNotConfigured, "server start")
		return m.err
	}

	m.echo = echo.New()
	m.echo.Debug = false
	m.echo.HideBanner = true
	m.echo.HidePort = true

	if m.useMetrics {
		m.echo.GET(metrics.Endpoint, echo.WrapHandler(promhttp.Handler()))
	}

	if m.useHealth {
		m.echo.GET(HealthEndpoint, echo.WrapHandler(m.handler))
	}

	return m.runHttpServer()
}

func (m *monitor) GracefulStop() error {
	defer m.once.Do(func() {
		close(m.serverErrors)
	})

	if m.echo == nil {
		return errors.Wrap(ErrServerNotSpecified, "stopping server")
	}

	if m.useHealth {
		m.handler.AddChecker("ShutDown", checkers.ShutDown())
	}

	ctx, cancel := context.WithTimeout(context.Background(), EchoShutdownTimeout)
	defer cancel()
	if err := m.echo.Shutdown(ctx); err != nil {
		m.err = errors.Wrap(err, "monitoring shutdown failed")
		return m.err
	}

	return nil
}

func (m *monitor) Error() error {
	return m.err
}

func (m *monitor) runHttpServer() error {
	m.isServerStarted = true

	go func(errorCh chan error) {
		if err := m.echo.Start(fmt.Sprintf("%s:%d", m.config.Host, m.config.Port)); err != nil {
			if err != http.ErrServerClosed {
				errorCh <- errors.Wrap(err, "monitoring server failure")
			}
		}
	}(m.serverErrors)

	select {
	case err := <-m.serverErrors:
		m.isServerStarted = false
		return err
	}
}
