.PHONY: all test lint
all: lint test

# Build variables
BIN_DIR ?= bin

lint:
	@golangci-lint run

test:
	@echo "Running tests"
	CGO_ENABLED=1 go test -tags "musl"  -p 1 -race  -cover ./...