package sentry

import (
	"net/http"
	"time"

	"github.com/evalphobia/logrus_sentry"
	"github.com/getsentry/raven-go"
	"gitlab.com/fcpartners/libs/logs/log"
)

const timeout = 200

type Config struct {
	DSN string
}

func SetUp(sentryConfig *Config, logger log.Logger) {
	client := raven.DefaultClient
	client.Transport.(*raven.HTTPTransport).Timeout = timeout * time.Millisecond
	client.Transport.(*raven.HTTPTransport).Transport.(*http.Transport).TLSClientConfig.InsecureSkipVerify = true
	err := client.SetDSN(sentryConfig.DSN)
	if err != nil {
		logger.Warn(err)
		return
	}

	hook, err := logrus_sentry.NewAsyncWithClientSentryHook(client, []log.Level{
		log.PanicLevel,
		log.FatalLevel,
		log.ErrorLevel,
		log.WarnLevel,
	})

	if err != nil {
		logger.Warn(err)
		return
	}

	hook.StacktraceConfiguration.Enable = true
	hook.StacktraceConfiguration.Level = log.ErrorLevel
	hook.StacktraceConfiguration.Context = 4
	hook.StacktraceConfiguration.SendExceptionType = true
	hook.StacktraceConfiguration.IncludeErrorBreadcrumb = true

	logger.AddHook(hook)
}
