package logs

import (
	"io/ioutil"

	"github.com/sirupsen/logrus"
	"gitlab.com/fcpartners/libs/logs/graylog"
	"gitlab.com/fcpartners/libs/logs/log"
	"gitlab.com/fcpartners/libs/logs/sentry"
	"gitlab.com/fcpartners/libs/logs/stdout"
)

type (
	LoggerConfig struct {
		Project   string
		Graylog   graylog.Config
		Sentry    sentry.Config
		ShowWhere bool
		//SkipLines int
	}

	logger struct {
		*logrus.Logger
		config      *LoggerConfig
		graylogHook graylog.HookManager
	}
)

func New(config *LoggerConfig) log.Logger {
	logger := &logger{
		Logger: logrus.StandardLogger(),
		config: config,
	}
	//logger.SetFormatter(&logrus.TextFormatter{})
	//logger.SetFormatter(&formatter{
	//	TimestampFormat: "2006-01-02 15:04:05.000",
	//	LogFormat:       "%time% [%lvl%] %msg% [%loc%]\n",
	//	LocationSkip:    config.SkipLine,
	//})
	logger.SetFormatter(&formatter{
		TimestampFormat: "2006-01-02 15:04:05.000",
		TrimMessages:    true,
		NoColors:        true,
		NoFieldsColors:  true,
		ShowFullLevel:   false,
		HideKeys:        false,
		ShowWhere:       config.ShowWhere,
		//SkipLines: config.SkipLines,
		//Project: config.Graylog.Project,
		Project: config.Project,
	})

	logger.SetNoLock()
	logger.SetOutput(ioutil.Discard)
	logger.SetReportCaller(true)

	AddStdOutLog(logger)
	AddSentryHook(&config.Sentry, logger)

	return logger
}

func (l *logger) SetUp() error {
	l.graylogHook = graylog.NewHookManager(&l.config.Graylog, l)
	l.graylogHook.SetUp()
	return nil
}

func (l *logger) TearDown() error {
	l.graylogHook.TearDown()
	return nil
}

func AddStdOutLog(logger log.Logger) {
	stdout.SetUp(logger)
}

func AddSentryHook(config *sentry.Config, logger log.Logger) {
	sentry.SetUp(config, logger)
}
